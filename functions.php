<?php
// Reverse WordPress' magic quotes on input variables
$_post = array_map('stripslashes_deep', $_POST);
$_get = array_map('stripslashes_deep', $_GET);
$_cookie = array_map('stripslashes_deep', $_COOKIE);
$_request = array_map('stripslashes_deep', $_REQUEST);

// Initialize session
session_start();

// Initialize Smarty
require_once dirname(__FILE__) . '/Smarty/libs/SmartyBC.class.php';
$smarty = new SmartyBC();
$smarty->setTemplateDir(dirname(__FILE__) . '/templates');
$smarty->setCompileDir(dirname(__FILE__) . '/templates_c');
$smarty->addPluginsDir(dirname(__FILE__) . '/plugins_wp');
$smarty->addPluginsDir(dirname(__FILE__) . '/plugins');
$smarty->left_delimiter = '{{';
$smarty->right_delimiter = '}}';
$smarty->php_handling = Smarty::PHP_ALLOW;

// Initialize smarty-wordpress framework
header('X-Powered-By: arwi.me', false);
foreach (glob(__DIR__ . "/plugins_wp/*.php") as $filename) {
    require_once $filename;
}
foreach (glob(__DIR__ . "/plugins/*.php") as $filename) {
    require_once $filename;
}
require_once dirname(__FILE__) . '/functions.inc.php';
