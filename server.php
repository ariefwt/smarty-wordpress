<?php
require_once dirname(__FILE__) . '/../../../wp-load.php';
$url = '';
$smarty_atts = array_merge($_SERVER, $wp_query->query_vars, $_FILES, $_request);
extract(wp_parse_args($smarty_atts, array(
    '_a' => ''
)));
if ($_a) {
    $url = call_user_func("smarty_server_$_a", $smarty_atts);
}
if (WP_DEBUG) {
    echo '<pre>';
    echo '<div>';
    print_r($smarty_atts);
    echo '</div>';
    echo "<div>URL=$url</div><div><a href='$url'>Click here to continue...</a></div>";
    echo '</pre>';
} else {
    header("Location: $url");
}
