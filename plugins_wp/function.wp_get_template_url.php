<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:	function.wp_get_template_url.php
 * Type:	function
 * Name:	wp_get_template_url
 * Purpose:	get template URL with its slug and optionally additional params
 * -------------------------------------------------------------
 */
function smarty_function_wp_get_template_url($params = array(), $template = null)
{
    extract(wp_parse_args($params, array(
        'assign' => '',
        'slug' => '',
        'params' => array()
    )));
    $query = http_build_query($params);
    $query = $query ? ('?' . $query) : '';
    $value = "/$slug/$query";
    if ($assign) {
        $template->assign($assign, $value);
        $value = null;
    }
    return $value;
}
?>