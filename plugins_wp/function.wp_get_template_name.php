<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:	function.wp_get_template_name.php
 * Type:	function
 * Name:	wp_get_template_name
 * Purpose:	get the template file name
 * -------------------------------------------------------------
 */
function smarty_function_wp_get_template_name($params = array(), $template = null)
{
    extract(wp_parse_args($params, array(
        'assign' => '',
        'default' => ''
    )));
    $value = trim(str_replace('/', '--', str_replace(home_url(), '', get_permalink())), '--');
    if (! $value) {
        $value = $default;
    }
    if ($assign) {
        $template->assign($assign, $value);
        $value = null;
    }
    return $value;
}
?>