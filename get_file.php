<?php
require_once dirname(__FILE__) . '/../../../wp-load.php';
$url = '';
$smarty_atts = array_merge($_SERVER, $wp_query->query_vars, $_FILES, $_request);
extract(wp_parse_args($smarty_atts, array(
    'id' => ''
)));
$sql_values = array();

/* The Query */
$rows = array();
$sql = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}data_files WHERE deleted_since = 0 AND id = %s LIMIT 1", array(
    $id
));
$row_offset = 0;
if ($row = $wpdb->get_row($sql, ARRAY_A, $row_offset ++)) {
    header("Content-Type: {$row['type']}");
    header("Content-Disposition: attachment; filename=\"{$row['name']}\"");
    readfile($tys_config['upload_dir'] . "/{$row['id']}");
}
