<?php
require_once dirname(__FILE__) . '/plugins_wp/function.wp_get_template_name.php';
$smarty_atts = array_merge($_SERVER, $wp_query->query_vars, $_FILES, $_request);
$smarty->assign('atts', $smarty_atts);
$smarty->assign('config', $smarty_config);
$smarty->assign('now', time());
$_tpl = 'pages/' . smarty_function_wp_get_template_name(array(
    'default' => 'home'
)) . '.tpl';
$smarty->display($smarty->templateExists($_tpl) ? $_tpl : 'app.tpl');
